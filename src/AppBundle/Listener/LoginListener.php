<?php

namespace AppBundle\Listener;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginListener
{

    /**
     * @var EntityManager
     */
    private $em;


    /**
     * @var
     */
    private $router;


    /**
     * @param EntityManager $em
     * @param Router $router
     */
    public function __construct(EntityManager $em, Router $router)
    {
        $this->em = $em;
        $this->router = $router;
    }

    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        /** @var User $user */
        if ($user) {
            $user->setLastLogin(new \DateTime());
            $this->em->persist($user);
            $this->em->flush($user);
        }
    }
}