<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use AppBundle\Entity\User;
use AppBundle\Entity\UserRoute;
use AppBundle\Form\Type\RouteFormType as RouteForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;


class RouteController extends Controller
{
    /**
     * @Route("/get/route", name="userRoutes")
     */
    public function getRouteAction(){
        /** @var User $user */
        $user = $this->getUser();
        $userRoutes = array();
        if($user){
            $routes = $user->getRoute();

            /** @var UserRoute $route */
            foreach($routes as $route){
                $userRoutes[] = array(
                    'id' => $route->getId(),
                    'name' => $route->getName(),
                    'coordinates' => json_decode($route->getValue(),true)
                );
            }

        }
        return new JsonResponse($userRoutes);
    }

    /**
     * @Route("/upload/route", name="uploadRoute")
     */
    public function uploadAction()
    {
        $form = $this->createUploadRouteForm();
        return $this->render('AppBundle:Route:index.html.twig', array(
            'uploadForm' => $form->createView()
        ));
    }

    private  function createUploadRouteForm(){
        $form = $this->createForm(new RouteForm(), new Document(), array(
            'action' => $this->generateUrl('upload'),
            'method' => 'POST'
        ));
        return $form;
    }

    /**
     * @Route("/routes", name="routes")
     */
    public function myRoutesAction()
    {
        $hasRoutes = $this->getUser() && $this->getUser()->getRoute()->count();

        return $this->render('AppBundle:Route:routes.html.twig', array(
            'hasRoutes' => $hasRoutes
        ));
    }

}
