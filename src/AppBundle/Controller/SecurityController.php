<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Login;
use Symfony\Component\HttpFoundation\Request;
use \FOS\UserBundle\Controller\SecurityController as BaseController;


class SecurityController extends BaseController
{

    public function loginAction(Request $request)
    {
        return parent::loginAction($request);
    }


    protected function renderLogin(array $data)
    {
       return $this->render('AppBundle:Security:login.html.twig', $data);
    }
}