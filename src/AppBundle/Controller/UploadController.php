<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use AppBundle\Entity\UserRoute;
use AppBundle\Form\Type\RouteFormType as RouteForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

class UploadController extends Controller
{
    /**
     * @Route("/upload", name="upload")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function uploadAction(Request $request){
        $flash = $this->get('braincrafted_bootstrap.flash');

        $document = new Document();

        $form = $this->createForm(new RouteForm(), $document);

        $form->handleRequest($request);

        if($form->isValid()){

            $file = $this->uploadFile($document, $form->getData()->getName());

            $fileContent = $this->getFileContent($file);

            try{
                $value = $this->parseFileContent($fileContent);

                $userRoute = new UserRoute();
                $userRoute->setName($form->getData()->getName());
                $userRoute->setUser($this->getUser());
                $userRoute->setValue($value);

                $this->getDoctrine()->getManager()->persist($userRoute);
                $this->getDoctrine()->getManager()->flush($userRoute);

                unlink($file);

                return $this->redirect($this->generateUrl('routes'));
            } catch(\Exception $e){
                $flash->error($e->getMessage());
            }

        }
        return $this->redirect($this->generateUrl('uploadRoute'));
    }


    /**
     * @param Document $document
     * @param $fileName
     * @return string file path
     */
    private function uploadFile(Document $document, $fileName){
        /** @var File $file */
        $file = $document->getFile();
        $dir = $this->container->getParameter('kernel.root_dir').'/../web/uploads/';
        $file->move($dir, $fileName);

        return $dir.'/'.$fileName;
    }

    /**
     * @param $file
     * @return string
     */
    private function getFileContent($file){
        return file_get_contents($file) ? file_get_contents($file) : '';
    }

    /**
     * @param $fileContent
     * @return string
     * @throws \Exception
     */
    private function parseFileContent($fileContent){
        $latAndLng = array();
        if(@simplexml_load_string($fileContent)){
            $xml = simplexml_load_string($fileContent);
            foreach($xml->trk->trkseg->trkpt as $obj){
                if(!( $obj && $obj[0]->attributes()->lat && $obj[0]->attributes()->lon)){
                    throw new \Exception('Gpx file is not valid. Missing some attributes!');
                    break;
                }
                $latAndLng[] = array(
                    'lat' => floatval($obj[0]->attributes()->lat),
                    'lng' => floatval($obj[0]->attributes()->lon)
                );
            }
            return json_encode($latAndLng,true);
        }
        throw new \Exception('Please upload valid gpx file');
    }

}
