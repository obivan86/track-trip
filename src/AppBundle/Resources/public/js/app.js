$(function(){
    var allRoutes = [];
    var initMap = function(coordinates){
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: {lat: coordinates[0].lat, lng: coordinates[0].lng},
            mapTypeId: google.maps.MapTypeId.TERRAIN
        });

        var routePath = new google.maps.Polyline({
            path: coordinates,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        routePath.setMap(map);
    };

    var getRoutes  = function(){
        var defferd = $.Deferred();
        $.ajax({
            url: "/get/route",
            type: 'GET',
            error: function(data) {
                defferd.reject(data);
            },
            success: function(data) {
                if(data.length){
                    allRoutes = data;
                    defferd.resolve(data[0].coordinates);
                    buildRouteList(allRoutes);
                }


            }
        });
        return defferd.promise();
    };

    var buildRouteList = function(routes){
        var $routeList = $('#routeList');
        for(var i=0; i< routes.length; i++){
            $routeList.append(
                '<a class="list-group-item route-name" data-id="'+ routes[i].id +'">'+ routes[i].name +'</a>'
            );
        }

        $routeList.find('a:first-child').addClass('active');

    };

    $.when(getRoutes()).done(function(response){
        initMap(response);
    }).fail(function(response){
        console.log(response);
    });

    $('#routeList').on('click', 'a', function(){
        $('#routeList a').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('data-id');
        var route = allRoutes.filter(function(item){
            return parseInt(item.id) == parseInt(id);
        })[0];
        initMap(route.coordinates);

    });
});
