<?php

namespace AppBundle\Form\Type;

use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends BaseType {

    const NAME = "custom_registration";

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return self::NAME;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', 'text', array(
            'label' => 'First Name',
            'attr' => array(
                'placeholder' => 'First Name'
            )
        ));

        $builder->add('lastName', 'text', array(
            'label' => 'Last Name',
            'attr' => array(
                'placeholder' => 'Last Name'
            )
        ));

        $builder->add('lastName', 'text', array(
            'label' => 'Last Name',
            'attr' => array(
                'placeholder' => 'Last Name'
            )
        ));

        $builder->add('email', 'email', array(
            'label' => 'Email',
            'attr' => array(
                'placeholder' => 'Email'
            )
        ));

        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'first_options' => array(
                'label' => 'Password'
            ),
            'second_options' => array(
                'label' => 'Confirm password'
            ),
            'invalid_message' => 'Please, enter same password',
        ));



        $builder->add('button', 'submit', array(
            'label' => 'Register'
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }



}