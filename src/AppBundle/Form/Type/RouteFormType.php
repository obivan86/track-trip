<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RouteFormType extends AbstractType {

    const NAME = "routeForm";

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return self::NAME;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'label' => 'Route name',
            'attr' => array(
                'placeholder' => 'Route Name'
            ),
            'required' => true
        ));

        $builder->add('file', 'file', array(
            'label' => 'Upload route',
            'required' => true
        ));

        $builder->add('button', 'submit', array(
            'label' => 'Upload'
        ));
    }
}