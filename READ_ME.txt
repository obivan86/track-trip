********
    Requirements: node npm
    Install uglifyjs, uglifycss and less using npm
    Example: npm install -g uglifyjs
********

Steps to start app

1. Open command line

2. Navigate to folder app

3. Run composer install - If you don't have installed composer first run curl -s https://getcomposer.org/installer | php

4. Populate config parameters required from composer

5. Run php app/console doctrine:database:create

6. Run php app/console doctrine:schema:update --force

7. Run php app/console app/console assets:install --symlink

8. Run php app/console app/console assetic:dump

9. Run php app/console server:start

Remark: App navigation is att the bottom